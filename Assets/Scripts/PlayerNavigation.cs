﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerNavigation : MonoBehaviour
{
    
    private Rigidbody rigidbody;
    public float speed, rotationSpeed;

    private Camera camera;


    private void Start()
    {
        rigidbody = GetComponent<Rigidbody>();
        camera = Camera.main;
    }

    private void Update()
    {
        float translation = Input.GetAxis("Vertical") * speed;
        float rotation = Input.GetAxis("Horizontal") * rotationSpeed;

        // Make it move 10 meters per second instead of 10 meters per frame...
        translation *= Time.deltaTime;
        rotation *= Time.deltaTime;

        // Move translation along the object's z-axis
        transform.Translate(0, 0, translation);

        // Rotate around our y-axis
        transform.Rotate(0, rotation, 0);


        // Get the mouse delta. This is not in the range -1...1
        float h = speed * Input.GetAxis("Mouse X");
        float v = speed * -Input.GetAxis("Mouse Y");

        if (camera.transform.rotation.x > -0.15f && camera.transform.rotation.x < 0.45f)
        {
            camera.transform.Rotate(v, 0, 0);
            Debug.Log(camera.transform.localRotation.x);
        }
    }
    
}
