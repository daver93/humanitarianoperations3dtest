﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerEyes : MonoBehaviour
{
    private Camera camera;
    private int layerMask = 1 << 10;

    public float raycastRange;

    private void Awake()
    {
        camera = Camera.main;
    }

    public void IsLookingAtObject(GameObject obj)
    {
        RaycastHit hit;

        if (Physics.Raycast(camera.transform.position, camera.transform.forward, out hit, raycastRange, layerMask))
        {
            if (hit.transform.gameObject.tag.Equals(obj.gameObject.tag) && Input.GetMouseButtonDown(0))
            {
                obj.GetComponent<DoorScript>().HandleDoor(true);
            }
        }
    }
}
