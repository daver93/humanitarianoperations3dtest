﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorScript : MonoBehaviour
{
    public bool isLocked;
    public bool isProximitySensorOn;

    private Animator animator;
    private Material material;
    private Color color;
    private Camera camera;
    private bool isOpen = false;

    private void Awake()
    {
        animator = GetComponent<Animator>();
        color = gameObject.GetComponent<Renderer>().material.color;
        camera = Camera.main;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag.Equals("Player") && !isOpen)
        {
            // Door is locked --> must get red when player is near
            if (isLocked && isProximitySensorOn)
            {
                gameObject.GetComponent<Renderer>().material.color = Color.red;
            }
            else if (isProximitySensorOn && !isLocked)
            {
                HandleDoor(true);
            }
            
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag.Equals("Player") && !isOpen)
        {
            // Door must open when player clicks on it
            if (!isLocked && !isProximitySensorOn)
            {
                camera.GetComponent<PlayerEyes>().IsLookingAtObject(gameObject);
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag.Equals("Player"))
        {
            // Door is locked --> must get the initial color when player is near
            if (isLocked && isProximitySensorOn)
            {
                gameObject.GetComponent<Renderer>().material.color = color;
            }
            else if (isProximitySensorOn && !isLocked)
            {
                HandleDoor(false);
            }
        }
    }

    public void HandleDoor(bool shouldOpen)
    {
        animator.SetBool("isOpen", shouldOpen);
        isOpen = shouldOpen;
    }

}
